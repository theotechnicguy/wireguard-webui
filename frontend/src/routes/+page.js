export async function load() {
	const response = await fetch("http://localhost:8080/peers");

	return {
		peers: await response.json(),
	};
}
