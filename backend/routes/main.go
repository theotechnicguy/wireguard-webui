/**
 * file: routes/main.go
 * author: @theotechniguy
 * license: Apache License 2.0
 *
 * This file contains the routes for the backend server.
 */

package routes

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/theotechnicguy/wireguard-webui/backend"
	"gitlab.com/theotechnicguy/wireguard-webui/backend/middleware"
)

var dataSource backend.DataSource

func getAllPeers(ctx *gin.Context) {
	peers, err := dataSource.GetAllPeers()
	if err != nil {
		ctx.JSON(500, gin.H{"error": err.Error()})
		return
	}

	ctx.JSON(http.StatusOK, peers)
}

// TODO: add remaining routes

func RegisterRoutes(router *gin.Engine, ds backend.DataSource) {
	dataSource = ds
	router.Use(middleware.CommonHeaders)
	router.GET("/peers", getAllPeers)
}
