/**
 * file: backend/connector_test.go
 * author: @theotechnicguy
 * license: Apache License 2.0
 *
 * This file contains the tests for the data source connector interface
 */

package backend_test

import (
	"fmt"
	"net"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/theotechnicguy/wireguard-webui/backend"
)

func TestNewAddressFromIPNet(t *testing.T) {
	_, ipNet, _ := net.ParseCIDR("192.168.0.1/24")

	address := backend.NewAddressFromIPNet(ipNet)

	assert.Equal(t, ipNet.IP, address.IP)

	expectedCIDR := 24
	assert.Equal(t, expectedCIDR, address.CIDR)
}

func TestAddressString(t *testing.T) {
	ip := net.ParseIP("192.168.0.1")
	address := backend.Address{
		IP:   ip,
		CIDR: 24,
	}

	expectedString := "192.168.0.1/24"
	assert.Equal(t, expectedString, address.String())
	assert.Equal(t, expectedString, fmt.Sprintf("%s", address))
}
