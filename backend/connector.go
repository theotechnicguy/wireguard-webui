/**
 * file: backend/connector.go
 * author: @theotechnicguy
 * license: Apache License 2.0
 *
 * This file contains the data source connector interface
 */

package backend

import (
	"fmt"
	"net"
	"time"
)

// The Statistics structure collects basic metrics concerning the peer.
// It's fields are read-only value.
type Statistics struct {
	LastHandshake time.Duration
	BytesReceived int
	BytesSent     int
}

// The Endpoint structure represents a WireGuard peer endpoint address.
// An Endpoint is basically an IP address or hostname with an associated port.
type Endpoint struct {
	Address string
	Port    int
}

// The Address structure is an easier to use alternative to net.IPNet.
// It uses a numerical CIDR instead of a binary mask.
type Address struct {
	IP   net.IP
	CIDR int
}

// NewAddressFromIPNet converts a net.IPNet to an Address, filling the fields
// as necessary.
func NewAddressFromIPNet(inet *net.IPNet) *Address {
	ones, _ := inet.Mask.Size()
	return &Address{
		IP:   inet.IP,
		CIDR: ones,
	}
}

// Address.String simply returns a string representation of the IP address
// in common CIDR notation, e.g.,
//
//	10.0.0.0/8
//	192.168.0.1/24
//	172.17.0.0/16
func (a Address) String() string {
	return fmt.Sprintf("%s/%d", a.IP.String(), a.CIDR)
}

// A Peer is a **remote** WireGuard device, the client that will connect from
// outside the network to be part of it.
type Peer struct {
	// A Peer's ID is it's unique reference at the data source. The data source
	// is the single source of truth and authority for the ID.
	// This field is read-only.
	ID uint

	// The AllowedAddresses field is the list of Addresses that the peer is
	// allowed to advertise and receive traffic to.
	// For simple devices, this is usually a single address with a CIDR of 32.
	// For more complex devices or remote networks this can be a list of
	// addresses with different CIDR values.
	// In any case, at least one address must be present, otherwise no traffic
	// will be allowed to flow.
	AllowedAddresses []*Address

	// A Peer's Owner is the account identifier of the person or entity
	// managing the peer.
	// This field is read-write and always required.
	Owner string

	// The Peer's Name is the human-readable name of the peer, set by the
	// the Owner.
	// This field is read-write and always required.
	Name string

	// When the Peer is connected, the CurrentEndpoint is the Endpoint from
	// where the peer is currently dialling-in.
	// When the Peer is not connected, it can be nil or the last known
	// active endpoint.
	// This field is set buy the data source and otherwise read-only and
	// optional.
	CurrentEndpoint *Endpoint

	// The ConfiguredEndpoint is the Endpoint from where the peer is supposed
	// to dial-in.
	// Generally, WireGuard will refuse connection from any other endpoint
	// than the one specified here.
	// This field is read-write and optional.
	ConfiguredEndpoint *Endpoint

	// If a Peer is Disabled, the configuration for the Peer is saved, but
	// connections are refused. It defaults to `false`.
	// This field is read-write and optional.
	Disabled bool

	// TODO: add the server

	// The KeepAlive duration specifies the time interval between keep-alive
	// packets. The WireGuard default is around 2 minutes.
	// This field is read-write and optional.
	KeepAlive time.Duration

	// The PublicKey (pk) is the public part of the WireGuard key pair.
	// This field is read-write and always required.
	PublicKey string

	// NOTE: The server should **never** have to deal with the secret key.
	// The secret key should be kept on the device and never shared (hence
	// the name).

	// The PreSharedKey (psk) is the shared secret common and known to both
	// the server and the peer. It enhances the security of the connection.
	// This field is read-write and optional.
	PreSharedKey string

	// The Peer's Statistics is a collection of read-only metrics set by the
	// data source. It's fields are read-only.
	Statistics Statistics
}

// The DataSource interface is used to define the data source
// connector interface. A data source is a location of WireGuard
// Peer records, generally the WireGuard server.
// The data source connector is only supposed to handle CRUD
// operations on the WireGuard peer records. All filtering and
// processing is done by the connector package.
type DataSource interface {
	// GetAllPeers returns a list of all known WireGuard peers,
	// no matter if they are enabled or disabled.
	GetAllPeers() ([]*Peer, error)

	// GetPeer returns a single WireGuard peer by its ID.
	GetPeer(id uint) (*Peer, error)

	// TODO: add other CRUD operations
}
