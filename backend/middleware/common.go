/**
 * file: backend/middleware/common.go
 * author: @theotechnicguy
 * license: Apache License 2.0
 *
 * The common middlewares file groups non-specific, general use
 * middlewares that can be used anywhere in the backend.
 */

package middleware

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

// CommonHeaders is a common middleware inserting common headers
// that should be included in every response from the server.
//
// More specifically, it sets Access Control headers, and the
// content-type option. It passes to the next handler afterwards.
func CommonHeaders(ctx *gin.Context) {
	ctx.Writer.Header().Set("Access-Control-Allow-Origin", "*")
	ctx.Writer.Header().Set("Access-Control-Allow-Headers", "Content-Type")
	ctx.Writer.Header().Set("Access-Control-Max-Age", "300")
	ctx.Writer.Header().Set("X-Content-Type-Options", "nosniff")
	ctx.Next()
}

// Terminate finishes the response and returns it to the client
// without content.
func Terminate(ctx *gin.Context) {
	ctx.AbortWithStatus(http.StatusNoContent)
}
