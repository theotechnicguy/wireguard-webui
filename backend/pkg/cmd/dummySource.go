/**
 * file: backend/pkg/cmd/dummySource.go
 * author: @theotechnicguy
 * license: Apache License 2.0
 *
 * This file contains a dummy data source implementation for testing purposes.
 */

package main

import (
	"fmt"

	"gitlab.com/theotechnicguy/wireguard-webui/backend"
)

type DummySource struct {
	peers []*backend.Peer
}

func NewDummySource() *DummySource {
	return &DummySource{
		peers: []*backend.Peer{
			{
				ID:               1,
				AllowedAddresses: []*backend.Address{{IP: []byte{10, 0, 0, 11}, CIDR: 32}},
				Owner:            "john.doe",
				Name:             "peer1",
				Disabled:         false,
				PublicKey:        "G47AkpA2pemr9ebSBZbQFNZwyhEkfj6RTjUzt7buX4w=",
				Statistics: backend.Statistics{
					LastHandshake: 0,
					BytesReceived: 0,
					BytesSent:     0,
				},
			},
			{
				ID:               2,
				AllowedAddresses: []*backend.Address{{IP: []byte{10, 0, 0, 12}, CIDR: 32}},
				Owner:            "jane.doe",
				Name:             "peer2",
				Disabled:         true,
				PublicKey:        "G47AkpA2pemr9ebSBZbQFNZwyhEkfj6RTjUzt7buX4w=",
				Statistics: backend.Statistics{
					LastHandshake: 0,
					BytesReceived: 0,
					BytesSent:     0,
				},
			},
			{
				ID:               3,
				AllowedAddresses: []*backend.Address{{IP: []byte{10, 0, 0, 13}, CIDR: 32}},
				Owner:            "john.doe",
				Name:             "peer3",
				Disabled:         false,
				PublicKey:        "G47AkpA2pemr9ebSBZbQFNZwyhEkfj6RTjUzt7buX4w=",
				PreSharedKey:     "G47AkpA2pemr9ebSBZbQFNZwyhEkfj6RTjUzt7buX4w=",
				Statistics: backend.Statistics{
					LastHandshake: 0,
					BytesReceived: 0,
					BytesSent:     0,
				},
			},
			{
				ID:               4,
				AllowedAddresses: []*backend.Address{{IP: []byte{10, 0, 0, 14}, CIDR: 32}},
				Owner:            "jane.doe",
				Name:             "peer4",
				Disabled:         true,
				PublicKey:        "G47AkpA2pemr9ebSBZbQFNZwyhEkfj6RTjUzt7buX4w=",
				PreSharedKey:     "G47AkpA2pemr9ebSBZbQFNZwyhEkfj6RTjUzt7buX4w=",
				Statistics: backend.Statistics{
					LastHandshake: 0,
					BytesReceived: 0,
					BytesSent:     0,
				},
			},
		},
	}
}

func (ds *DummySource) GetAllPeers() ([]*backend.Peer, error) {
	return ds.peers, nil
}

func (ds *DummySource) GetPeer(id uint) (*backend.Peer, error) {
	for _, peer := range ds.peers {
		if peer.ID == id {
			return peer, nil
		}
	}

	// TODO: return real error
	return nil, fmt.Errorf("peer with ID %d not found", id)
}
