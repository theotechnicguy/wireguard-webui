/**
 * file: backend/pkg/cmd/main.go
 * author: @theotechniguy
 * license: Apache License 2.0
 *
 * This file contains the main entry point for the backend server.
 */

package main

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/theotechnicguy/wireguard-webui/backend/routes"
)

func main() {
	router := gin.Default()
	ds := NewDummySource()
	routes.RegisterRoutes(router, ds)
	router.Run(":8080")
}
